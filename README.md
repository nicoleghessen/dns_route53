**DNS and Route 53**
- domain name system
name --> server 
www.bbc.co.uk --> points to 43.21.2.2

it's a system to assign web address/domains to servers IPs

DNS Service Providers:
- Godaddy.com
- gandi.net
- godomain.com
- namecheap.pt

These providers allow you to register (buy) a domain

ICANN- manages the assignment of domain names, registers the DNS service providers requests.
https://lookup/icann.org/

When you register a domain, you control it's records.

### DNS Records

When you buy a domain, you set the records, the records are replicated worldwide.
Check the records using DNS checker (www.dnschecker.org) these records help you to manage your domain 


<u>There are 8 records:</u>
```
 - A
 - AAAA
 - CNAME
 - PTR
 - NS
 - MX
 - SOA
 - TXT
```

**A** records: points to an IPv4 address

**AAAA** records: enable client devices to learn the IP address for a domain name. The IP address isn’t a typical IPv4 address, instead, AAAA records point to IPv6 addresses.

**CNAME** Conical Name record: to set alias- must always point to a domain not an IP. These records are typically used to point multiple hosts to a single location, without having to specifically assign an A record to each hostname e.g. name.com --> othername.com so you can set several domains to one!

**NS** records: 'NameServer' and the nameserver record indicates which DNS server is authoritative for that domain (priority & security)

**MX** records: which server can send emails with this name.

### DNS and AWS

Route 53: 
- AWS it manages DNS's + instantaneous DNS propagation
- DNS according to GeoLocation: you can have route 53 and send you in different locations depending to where you are. Helps you deal with compliance and reduces latency (time waiting for two servers to communicate- signal)

Port 8080 is a development port so need to specify that after the url as it won't work otherwise.


### Branching

Branching timelines off the master timeline
Creating several branches from one timeline.
```bash
# create a new branch from master
# step 0: you need to be in master and should be updated
$ git pull origin master

# create your new branch- use good naming convention
$ git checkout -b <branch_name>

## name convention differs based on the team 
# one good thing is start with dev-feature

$ git checkout -m dev-front- page

 make changes, git add and git commit.
 # Once ready, push your branch 






```


## Extra- Branching notes

### name convention differs based on the team 
# one good thing is start with dev-feature

$ git checkout -m dev-front- page

having naming convention can help you build actions on Jenkins

targets anything saying dev-* to trigger CI

## make another job to listen to deploy -*

$ git log- gives you names of commit and shows you the hash.

"testing 1 for branch"